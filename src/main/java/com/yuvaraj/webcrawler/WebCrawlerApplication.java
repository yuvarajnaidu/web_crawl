package com.yuvaraj.webcrawler;

import com.yuvaraj.webcrawler.service.WebCrawlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebCrawlerApplication implements CommandLineRunner {

    @Autowired
    WebCrawlService webCrawlService;

    public static void main(String[] args) {
        SpringApplication.run(WebCrawlerApplication.class, args);
        System.out.println("\n\n\n\n\n");

    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("\n\n\n");
        System.out.println("Reading URL");
        boolean check = false;
        if (args.length > 0) {
            check = true;
            for (int i = 0; i < args.length; i++) {
                webCrawlService.analyzeWebUrl(args[i]);
                System.out.println("\n\n");
                System.out.println(args[i] + " Analyzing complete");
            }
        } else {
            System.out.println("Please Add URL Argument");
        }
        if (check) {
            System.out.println(webCrawlService.printTableInConsole());
        }

    }
}
