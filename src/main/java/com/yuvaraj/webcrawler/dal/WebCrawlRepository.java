package com.yuvaraj.webcrawler.dal;

import com.yuvaraj.webcrawler.model.WebCrawl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebCrawlRepository extends JpaRepository<WebCrawl,String> {



}
