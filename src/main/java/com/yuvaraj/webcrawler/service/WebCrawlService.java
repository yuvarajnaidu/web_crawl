package com.yuvaraj.webcrawler.service;

import com.yuvaraj.webcrawler.model.WebCrawl;

public interface WebCrawlService {

    void analyzeWebUrl(String webUrl) throws Exception;
    void saveWebCrawl(WebCrawl webCrawl, String id) throws Exception;
    String printTableInConsole() throws Exception;
}
