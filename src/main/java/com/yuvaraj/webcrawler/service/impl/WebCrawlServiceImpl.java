package com.yuvaraj.webcrawler.service.impl;

import com.yuvaraj.webcrawler.dal.WebCrawlRepository;
import com.yuvaraj.webcrawler.model.ExtraItem;
import com.yuvaraj.webcrawler.model.ItemContent;
import com.yuvaraj.webcrawler.model.WebCrawl;
import com.yuvaraj.webcrawler.service.WebCrawlService;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class WebCrawlServiceImpl implements WebCrawlService {

    @Autowired
    WebCrawlRepository repository;

    private ItemContent itemContent;
    private ExtraItem extraItem;

    @Override
    public void analyzeWebUrl(String webUrl) throws Exception {
        //Getting WebPage
        Document doc = Jsoup.connect(webUrl).userAgent("Mozilla").header("Cache-control", "no-cache").header("Cache-store", "no-store").timeout(10000).get();

        //Elements
        Element page_title = doc.getElementsByClass("page-title").select("span").first();
        Element price = doc.getElementsByClass("price").first();
        Elements item_content_title = doc.getElementsByClass("data item title");
        Elements item_contents = doc.getElementsByClass("data item content");
        Elements sizes = doc.getElementsByTag("script");

        WebCrawl webCrawl = new WebCrawl();
        webCrawl.setProductName(page_title.text());
        Set<ItemContent> itemContentSet = new HashSet<>();

        itemContent = new ItemContent();
        itemContent.setLabel("Price");
        itemContent.setValue(price.text());
        itemContent.setWebCrawl(webCrawl);
        itemContentSet.add(itemContent);
        //Getting Details and More Information
        int item_content_index = 0;
        for (Element element : item_content_title) {
            if (!element.text().contains("Reviews")) {
                itemContent = new ItemContent();
                itemContent.setLabel(element.text());
                itemContent.setValue(item_contents.get(item_content_index).text());
                itemContent.setWebCrawl(webCrawl);
                itemContentSet.add(itemContent);
                item_content_index++;
            } else {
                Elements review = doc.getElementsByTag("script");
                for (Element reviewElement : review) {
                    if (reviewElement.html().contains("dataProductIdSelector")) {
                        String id = reviewElement.html().substring(
                                (reviewElement.html().indexOf("dataProductIdSelector = '") + 25),
                                (reviewElement.html().indexOf("',\n" +
                                        "            priceBoxes") - 1));
                        id = id.substring(id.indexOf("=") + 1, id.length());
                        Document reviewJson = Jsoup.connect("http://magento-test.finology.com.my/review/product/listAjax/id/" + id + "/").get();
                        Elements li = reviewJson.getElementsByClass("item review-item");
                        AtomicInteger i = new AtomicInteger(1);
                        li.forEach(data -> {
                            itemContent = new ItemContent();
                            extraItem = new ExtraItem();
                            Set<ExtraItem> extraItemSet = new HashSet<>();
                            itemContent.setLabel("Review " + (i.getAndIncrement()));
                            itemContent.setValue("");
                            itemContent.setExtraItem(true);
                            itemContent.setWebCrawl(webCrawl);
                            extraItem.setLabel(data.getElementsByClass("review-content").text());
                            extraItem.setValue(data.getElementsByClass("review-details").text());
                            extraItem.setItemContent(itemContent);
                            extraItemSet.add(extraItem);
                            extraItem = new ExtraItem();
                            extraItem.setLabel(data.getElementsByClass("review-title").text());
                            extraItem.setValue(data.getElementsByClass("review-ratings").text());
                            extraItem.setItemContent(itemContent);
                            extraItemSet.add(extraItem);
                            itemContent.setExtraItemSet(extraItemSet);
                            itemContentSet.add(itemContent);
                        });
                    }
                }
            }
        }
        //Related Products Getting
        Elements relatedProducts = doc.getElementsByClass("item product product-item");
        itemContent = new ItemContent();
        Set<ExtraItem> relatedProductSet = new HashSet<>();
        for (int i = 0; i < relatedProducts.size(); i++) {
            itemContent.setLabel("Related Products");
            extraItem = new ExtraItem();
            extraItem.setLabel(relatedProducts.get(i).getElementsByClass("product-item-link").text());
            extraItem.setValue(relatedProducts.get(i).getElementsByClass("price").text());
            extraItem.setItemContent(itemContent);
            relatedProductSet.add(extraItem);
            itemContent.setWebCrawl(webCrawl);
            itemContent.setExtraItemSet(relatedProductSet);
            itemContent.setExtraItem(true);
            itemContent.setValue("");
        }
        itemContentSet.add(itemContent);

        //Getting Size And Color via json found in script
        String sizeAndColor = null;
        for (int i = 0; i < sizes.size(); i++) {
            if (sizes.get(i).html().contains("jsonSwatchConfig")) {
                int start = sizes.get(i).html().indexOf("\"jsonSwatchConfig\"");
                int end = sizes.get(i).html().indexOf("\"mediaCallback\"");
                sizeAndColor = sizes.get(i).html().substring(start, end);
            }
        }
        if (sizeAndColor != null) {
            sizeAndColor = sizeAndColor.substring(sizeAndColor.indexOf(":") + 1);
        }

        //Mapping so that easier to store objects in string mode
        Map<String, List<String>> sizeAndColorMap = new HashMap<>();
        JSONObject jsonObject = new JSONObject(sizeAndColor);
        Iterator<String> keys = jsonObject.keys();
        for (Iterator<String> it = keys; it.hasNext(); ) {
            String key = it.next();
            List<String> variable = new ArrayList<>();
            for (Iterator<String> childKey = jsonObject.getJSONObject(key).keys(); childKey.hasNext(); ) {
                String childKeyVariable = childKey.next();
                variable.add(jsonObject.getJSONObject(key).getJSONObject(childKeyVariable).getString("label"));
            }
            sizeAndColorMap.put(key, variable);
        }
        for (String name : sizeAndColorMap.keySet()) {
            itemContent = new ItemContent();
            if (name.equals("145")) {
                itemContent.setLabel("Size");
                itemContent.setValue(sizeAndColorMap.get(name).toString());
                itemContent.setWebCrawl(webCrawl);
            } else if (name.equals("93")) {
                itemContent.setLabel("Color");
                itemContent.setValue(sizeAndColorMap.get(name).toString());
                itemContent.setWebCrawl(webCrawl);
            }
            itemContentSet.add(itemContent);
        }
        webCrawl.setLabelValue(itemContentSet);

        //Saving
        saveWebCrawl(webCrawl, page_title.text());
    }

    public String printTableInConsole() throws Exception {
        List<WebCrawl> webCrawlList = repository.findAll().stream().sorted(Comparator.comparing(WebCrawl::getProductName)).collect(Collectors.toList());
        AsciiTable asciiTable = new AsciiTable();
        webCrawlList.forEach(webCrawl -> {
            List<ItemContent> itemContentList = new ArrayList<ItemContent>(webCrawl.getLabelValue()).stream().sorted(Comparator.comparing(ItemContent::getLabel)).collect(Collectors.toList());
            asciiTable.addRule();
            asciiTable.addRow(null, null, webCrawl.getProductName()).setTextAlignment(TextAlignment.CENTER);
            asciiTable.addRule();
            asciiTable.addRow("Label", "Value", "").setTextAlignment(TextAlignment.CENTER);
            asciiTable.addRule();
            for (ItemContent itemContent : itemContentList) {
                if (itemContent.isExtraItem()) {
                    Set<ExtraItem> extraItemSet = new HashSet<>(itemContent.getExtraItemSet());
                    asciiTable.addRow(itemContent.getLabel(), "Label", "Value").setTextAlignment(TextAlignment.CENTER);
                    asciiTable.addRule();
                    for (ExtraItem extraItem : extraItemSet) {
                        asciiTable.addRow("", extraItem.getLabel(), extraItem.getValue()).setTextAlignment(TextAlignment.CENTER);
                        asciiTable.addRule();
                    }
                } else {
                    asciiTable.addRow(itemContent.getLabel(), itemContent.getValue(), "").setTextAlignment(TextAlignment.CENTER);
                    asciiTable.addRule();
                }
            }
        });
        return asciiTable.render();
    }

    @Transactional
    public void saveWebCrawl(WebCrawl webCrawl, String id) throws Exception {
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
            repository.save(webCrawl);
        } else {
            repository.save(webCrawl);
        }
    }
}
