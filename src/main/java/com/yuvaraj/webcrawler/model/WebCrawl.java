package com.yuvaraj.webcrawler.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "web_crawl")
public class WebCrawl implements Serializable {

    @Id
    @Column(name="product_name",nullable=false)
    @org.springframework.data.annotation.Id
    private String productName;

    @OneToMany(fetch = FetchType.EAGER,mappedBy="webCrawl",cascade = CascadeType.ALL)
    private Set<ItemContent> labelValue = new HashSet<ItemContent>();

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Set<ItemContent> getLabelValue() {
        return labelValue;
    }

    public void setLabelValue(Set<ItemContent> labelValue) {
        this.labelValue = labelValue;
    }
    }
