package com.yuvaraj.webcrawler.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "item_content")
public class ItemContent implements Serializable {

    @Id @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;

    @Column(name="label",nullable=false)
    private String label;

    @Column(name="value",nullable=false)
    private String value;

    @Column(name="extra_item")
    private boolean extraItem;

    @OneToMany(fetch = FetchType.EAGER,mappedBy="itemContent",cascade = CascadeType.ALL)
    private Set<ExtraItem> extraItemSet = new HashSet<ExtraItem>();

    @ManyToOne
    @JoinColumn(name ="FK_WebCrawlId")
    private WebCrawl webCrawl;

    public boolean isExtraItem() {
        return extraItem;
    }

    public void setExtraItem(boolean extraItem) {
        this.extraItem = extraItem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Set<ExtraItem> getExtraItemSet() {
        return extraItemSet;
    }

    public void setExtraItemSet(Set<ExtraItem> extraItemSet) {
        this.extraItemSet = extraItemSet;
    }

    public WebCrawl getWebCrawl() {
        return webCrawl;
    }

    public void setWebCrawl(WebCrawl webCrawl) {
        this.webCrawl = webCrawl;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
