package com.yuvaraj.webcrawler.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "extra_item")
public class ExtraItem implements Serializable {

    @Id @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "uuid2")
    private String id;

    @Column(name="label",nullable=false)
    private String label;

    @Column(name="value",nullable=false)
    private String value;

    @ManyToOne
    @JoinColumn(name ="FK_ItemContentId")
    private ItemContent itemContent;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }


    public ItemContent getItemContent() {
        return itemContent;
    }

    public void setItemContent(ItemContent itemContent) {
        this.itemContent = itemContent;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
