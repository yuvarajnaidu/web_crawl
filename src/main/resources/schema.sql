create table if not exists web_crawl
(
	product_name varchar(255) not null
		constraint web_crawl_pk
			primary key
);
create table if not exists extra_item
(
	id varchar(255) not null
		constraint extra_item_pk
			primary key
		constraint fk_item_content_id
			references item_content (id),
	label varchar(255) not null,
	value varchar(255) not null
);

create table if not exists  item_content
(
	id varchar(255) not null
		constraint item_content_pk
			primary key
		constraint fk_web_crawl_id
			references web_crawl (product_name),
	extra_item boolean,
	label varchar(255) not null,
	value varchar(255)
);



